all: cv.pdf

cv: cv.pdf
	

resume: resume.pdf
	

resume.pdf: resume.tex
	pdflatex resume.tex

resume-google: resume-google.pdf
	

resume-google.pdf: resume-google.tex
	pdflatex resume-google.tex

cv.pdf: cv.tex
	xelatex cv.tex

clean:
	rm -f *.aux *.log *.out
